(function () {
  var request = require('request');
  var Promise = require('promise');
  var deepEqual = require('deep-equal');

  var API = 'https://demo-structure.almworks.com/rest/structure/2.0';

  var requestCounter = 0;

  // runtime parameters
  // structure ID we're working with
  var structureId;
  // current version of forest from the structure -- needed for updates
  var forestVersion = {signature: 0, version: 0};
  // current version of "item update stream" -- not used here, but tracked
  var itemsVersion = {signature: 0, version: 0};
  // selection of rows at the top level of the structure
  var topRows;


  Promise.resolve()
    .then(createNewStructure)
    .then(renameStructure)
    .then(poll)
    .then(addInserter)
    .then(addGrouper)
    .then(getTopRowIds)
    .then(getValuesForTopRows)
    .catch(function (error) {
      console.log(error);
    });


  // Creates a new structure. At least the name should be provided.
  function createNewStructure() {
    return post(API + '/structure', {name: 'sample structure'})
      .then(function (r) {
        structureId = r.id;
        return r;
      });
  }

  // Updates one or more structure properties.
  function renameStructure() {
    return post(API + '/structure/' + structureId + '/update', {name: 'sample structure #' + structureId});
  }

  // Create generator: inserter that adds issues according to JQL.
  function addInserter() {
    return post(API + '/item/create', {
      // Forest spec and version, where item is created. Version must have up-to-date signature, but be somewhat
      // outdated.
      forest: {spec: {structureId: structureId}, version: forestVersion},
      // Items version for receiving items update.
      items: {version: itemsVersion},
      // Ignore this, but the field should be present
      parameters: {},
      // Any negative number that represents temporary row ID of the item.
      rowId: -100,
      // (under, after, before) are row ID based coordinates of where the new item should be inserted.
      // Use (0, 0, 0) to insert at the top level as the first item.
      under: 0,
      after: 0,
      before: 0,
      // The item parameters
      item: {
        // Item type. Current supported for creation: folder, generator, issue, Confluence page
        type: 'com.almworks.jira.structure:type-generator',
        // Value map for the new item. In this case, generator parameters
        values: {
          // Generator's complete module key.
          moduleKey: 'com.almworks.jira.structure:inserter-jql',
          // Parameters for the generator.
          params: {
            // JQL
            jql: 'project = HDE',
            // Maximum amount of issues
            limit: 1000
          }
        }
      }
    })
      .then(updateVersions);
  }

  // Same call as addInserter - but this time adding a grouper by Assignee.
  function addGrouper() {
    return post(API + '/item/create', {
      forest: {spec: {structureId: structureId}, version: forestVersion},
      items: {version: itemsVersion},
      parameters: {},
      rowId: -100,
      under: 0,
      after: 0,
      before: 0,
      item: {
        type: 'com.almworks.jira.structure:type-generator',
        values: {
          moduleKey: "com.almworks.jira.structure:grouper-field",
          params: {
            fieldId: "assignee"
          }
        }
      }
    })
      .then(updateVersions);
  }


  // Retrieves full forest and gets row ids for the top level.
  function getTopRowIds() {

    function Row(rowId, depth, itemId) {
      this.rowId = rowId;
      this.depth = depth;
      this.itemId = itemId;
    }


    return get(API + '/forest/latest?s=' + encodeURIComponent(JSON.stringify({structureId: structureId})))
      .then(function (r) {
        var formula = r.formula;
        var itemTypes = r.itemTypes;

        // formula parsing
        var rowFormulas = formula.split(',');
        var rows = rowFormulas.map(function (rf) {
          var parts = rf.split(':');
          var rowId = parseInt(parts[0], 10);
          var depth = parseInt(parts[1], 10);

          var itemId = parts[2];
          // replace numeric item type id with full item type id
          var k = itemId.indexOf('/');
          if (k < 0) {
            // if there's no type, then it is issue
            itemId = 'com.almworks.jira.structure:type-issue/' + itemId;
          } else {
            itemId = itemTypes[itemId.substring(0, k)] + itemId.substring(k);
          }
          return new Row(rowId, depth, itemId);
        });

        topRows = rows.filter(function (row) {
          return row.depth == 0 && row.itemId.indexOf('com.almworks.jira.structure:type-generator') < 0;
        });
      });
  }

  // Requests values based on the forest spec, row IDs and attribute spec.
  function getValuesForTopRows() {
    // Some basic attributes:
    var SUMMARY_ATTRIBUTE = {
      id: 'summary',
      format: 'text'
    };
    var COUNT_ATTRIBUTE = {
      id: 'count',
      format: 'number',
      params: {
        // don't count parents
        leaves: true
      }
    };
    var TOTAL_TIME_SPENT = {
      id: 'sum',
      format: 'duration',
      params: {
        attribute: {
          id: 'timespent',
          format: 'duration'
        }
      }
    };

    return post(API + '/value', {
      requests: [{
        forestSpec: {structureId: structureId},
        rows: topRows.map(function (r) { return r.rowId; }),
        attributes: [
          SUMMARY_ATTRIBUTE, COUNT_ATTRIBUTE, TOTAL_TIME_SPENT
        ]
      }]
    })
      .then(function(r) {
        // Parsing /value response - a bit complicated. The response contains parallel arrays, with "rows" array
        // identifying the order.
        
        var rows = r.responses[0].rows;
        var data = r.responses[0].data;

        // Create array of objects, one for each row. We'll collect values in this table.
        var table = rows.map(function(rowId) { return { rowId: rowId}; });

        data.forEach(function(valueSet) {
          var key;
          if (deepEqual(SUMMARY_ATTRIBUTE, valueSet.attribute)) {
            key = 'summary';
          } else if (deepEqual(COUNT_ATTRIBUTE, valueSet.attribute)) {
            key = 'count';
          } else if (deepEqual(TOTAL_TIME_SPENT, valueSet.attribute)) {
            key = 'totalTime';
          } else {
            return;
          }

          var values = valueSet.values;
          for (var i = 0; i < table.length; i++) {
            table[i][key] = values[i];
          }
        });


        // output table as CSV
        console.log('\n\nROW ID;ASSIGNEE;NUMBER OF TASKS;TOTAL TIME (MILLIS)');
        for (var i = 0; i < table.length; i++) {
          var row = table[i];
          console.log(row.rowId + ';"' + row.summary + '";' + row.count + ';' + row.totalTime);
        }
      });
  }



  function poll() {
    return post(API + '/poll', {
        forests: [
          // one can poll multiple forests at once
          {
            // spec is the "forest spec" - see wiki and javadoc
            spec: {
              structureId: structureId
            },
            // version is the last known forest version, or use { signature: 0, version: 0 } to get full update
            version: forestVersion
          }
        ],
        items: {
          // version is the last known version of items sequence, or use { signature: 0, version: 0 } to get full update
          version: itemsVersion
        }
      }
    ).then(updateVersions);
  }

  function updateVersions(reply) {
    forestVersion = reply.forestUpdates[0].version;
    itemsVersion = reply.itemsUpdate.version;
    return reply;
  }

  function get(url) {
    return req({
      url: url,
      method: 'GET'
    });
  }

  function post(url, body) {
    return req({
      url: url,
      method: 'POST',
      body: body
    });
  }


  function req(params) {
    var count = ++requestCounter;
    return new Promise(function (resolve, reject) {
      console.log(count + ' ==> ' + params.method + ' ' + params.url);
      if (params.body) console.log('      ' + JSON.stringify(params.body));

      params.auth = {
        username: 'demo',
        password: 'demo',
        sendImmediately: true
      };
      params.json = true;

      request(params, function (error, response, body) {
        console.log(count + ' <== ' + JSON.stringify(body));
        resolve(body);
      });
    });
  }

})();

